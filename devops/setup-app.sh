# Set up Laravel after main deployment. Called from CodeDeploy's
# appspec.yml.

# Move the previously downloaded .env file to the right place.
mv /tmp/production.env /var/www/app/.env
# mv /tmp/oauth-public.key /var/www/app/storage/oauth-public.key
# mv /tmp/oauth-private.key /var/www/app/storage/oauth-private.key

# Create the storage directories.
sudo mkdir -p /var/www/app/bootstrap/cache
sudo mkdir -p /var/www/app/storage/app/public
sudo mkdir -p /var/www/app/storage/logs
sudo mkdir -p /var/www/app/storage/framework/{sessions,views,cache}

# Run new migrations. While this is run on all instances, only the
# first execution will do anything. As long as we're using CodeDeploy's
# OneAtATime configuration we can't have a race condition.
sudo php /var/www/app/artisan migrate --force

# Run production optimizations.
sudo php /var/www/app/artisan config:cache
sudo php /var/www/app/artisan optimize
sudo php /var/www/app/artisan route:cache
sudo chown -R apache:apache /var/www/app/storage
sudo chown -R ec2-user:apache /var/www/app/bootstrap/cache
sudo rm -fr /var/www/app/.git

# Reload php-fpm to clear OPcache.
sudo systemctl restart php-fpm
sudo systemctl restart nginx

touch /tmp/deployment-done
